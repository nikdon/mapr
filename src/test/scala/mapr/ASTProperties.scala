package mapr

import mapr.Tree.{ Identifier, Op, Statement }
import org.scalacheck.Properties
import org.scalacheck.Prop._

object OutProperties extends Properties("Out") {
  property("out") = AST.parseStatement("out a123") ?= Tree.Statement(None, Tree.Out(Tree.Identifier("a123")))
}

object PrintProperties extends Properties("Print") {
  property("print") = AST.parseStatement("print \"test=\"") ?= Tree.Statement(None, Tree.Print("test="))
}

object StatementProperties extends Properties("Statement") {
  property("int") = AST.parseStatement("val i = 1") ?= Tree.Statement(Some("i"), Tree.Int(1))
  property("(int)") = AST.parseStatement("val a = (1)") ?= Tree.Statement(Some("a"), Tree.Int(1))

  property("int,op,int") = AST.parseStatement("val i=1+2") ?= Tree.Statement(
    Some("i"),
    Tree.Op(Tree.Int(1), Token.Ops.Plus, Tree.Int(2))
  )
  property("int,op,(int)") = AST.parseStatement("val i=1+(2)") ?= Tree.Statement(
    Some("i"),
    Tree.Op(Tree.Int(1), Token.Ops.Plus, Tree.Int(2))
  )
  property("(int),op,(int)") = AST.parseStatement("val i=(1)+(2)") ?= Tree.Statement(
    Some("i"),
    Tree.Op(Tree.Int(1), Token.Ops.Plus, Tree.Int(2))
  )
}

object RangeProperties extends Properties("Range") {
  property("simple.int,int") = AST.parseStatement("val r={1,2}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Int(1), Tree.Int(2))
  )
  property("simple.(int),int") = AST.parseStatement("val r={(1),2}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Int(1), Tree.Int(2))
  )
  property("simple.(int),(int)") = AST.parseStatement("val r={(1),(2)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Int(1), Tree.Int(2))
  )
  property("simple.expr,int") = AST.parseStatement("val r={1+1,2}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Op(Tree.Int(1), Token.Ops.Plus, Tree.Int(1)), Tree.Int(2))
  )
  property("simple.expr,(int)") = AST.parseStatement("val r={1+1,(2)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Op(Tree.Int(1), Token.Ops.Plus, Tree.Int(1)), Tree.Int(2))
  )
  property("simple.expr,expr") = AST.parseStatement("val r={1+1,2+2}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Op(Tree.Int(1), Token.Ops.Plus, Tree.Int(1)), Tree.Op(Tree.Int(2), Token.Ops.Plus, Tree.Int(2)))
  )
  property("simple.expr,expr") = AST.parseStatement("val r={(1+1),(2+2)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Op(Tree.Int(1), Token.Ops.Plus, Tree.Int(1)), Tree.Op(Tree.Int(2), Token.Ops.Plus, Tree.Int(2)))
  )
  property("simple.expr,expr") = AST.parseStatement("val r={1+1,2+(2+2)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(
      Tree.Op(Tree.Int(1), Token.Ops.Plus, Tree.Int(1)),
      Tree.Op(Tree.Int(2), Token.Ops.Plus, Tree.Op(Tree.Int(2), Token.Ops.Plus, Tree.Int(2)))
    )
  )
  property("simple.id,expr") = AST.parseStatement("val r={a,2+2}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Identifier("a"), Tree.Op(Tree.Int(2), Token.Ops.Plus, Tree.Int(2)))
  )
  property("simple.id,id") = AST.parseStatement("val r={a,b}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Identifier("a"), Tree.Identifier("b"))
  )
  property("simple.(id),(id)") = AST.parseStatement("val r={(a),(b)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Identifier("a"), Tree.Identifier("b"))
  )

  property("simple.float,float") = AST.parseStatement("val r={1.0,2.0}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Float(1), Tree.Float(2))
  )
  property("simple.(float),float") = AST.parseStatement("val r={(1.0),2.0}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Float(1), Tree.Float(2))
  )
  property("simple.(float),(float)") = AST.parseStatement("val r={(1.0),(2.0)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Float(1), Tree.Float(2))
  )
  property("simple.expr.float,float") = AST.parseStatement("val r={1.0+1.0,2.0}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Op(Tree.Float(1), Token.Ops.Plus, Tree.Float(1)), Tree.Float(2))
  )
  property("simple.expr.float,(float)") = AST.parseStatement("val r={1.0+1.0,(2.0)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Op(Tree.Float(1), Token.Ops.Plus, Tree.Float(1)), Tree.Float(2))
  )
  property("simple.expr.float,expr.float") = AST.parseStatement("val r={1.0+1.0,2.0+2.0}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(
      Tree.Op(Tree.Float(1), Token.Ops.Plus, Tree.Float(1)),
      Tree.Op(Tree.Float(2), Token.Ops.Plus, Tree.Float(2))
    )
  )
  property("simple.expr.float,expr.float") = AST.parseStatement("val r={(1.0+1.0),(2.0+2.0)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(
      Tree.Op(Tree.Float(1), Token.Ops.Plus, Tree.Float(1)),
      Tree.Op(Tree.Float(2), Token.Ops.Plus, Tree.Float(2))
    )
  )
  property("simple.expr.float,expr.float") = AST.parseStatement("val r={1.0+1.0,2.0+(2.0+2.0)}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(
      Tree.Op(Tree.Float(1), Token.Ops.Plus, Tree.Float(1)),
      Tree.Op(Tree.Float(2), Token.Ops.Plus, Tree.Op(Tree.Float(2), Token.Ops.Plus, Tree.Float(2)))
    )
  )
  property("simple.id,expr.float") = AST.parseStatement("val r={a,2.0+2.0}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Identifier("a"), Tree.Op(Tree.Float(2), Token.Ops.Plus, Tree.Float(2)))
  )

  property("simple.id,expr.int/float") = AST.parseStatement("val r={a,2+2.0}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Identifier("a"), Tree.Op(Tree.Int(2), Token.Ops.Plus, Tree.Float(2)))
  )
  property("simple.id,expr.float/int") = AST.parseStatement("val r={a,2.0+2}") ?= Tree.Statement(
    Some("r"),
    Tree.Range(Tree.Identifier("a"), Tree.Op(Tree.Float(2), Token.Ops.Plus, Tree.Int(2)))
  )
}

object MapProperties extends Properties("Map") {
  property("simple.map") = AST.mapNode.parse("map({0,1},i->i+1)").get.value ?= Tree.Map(
    Tree.Range(Tree.Int(0), Tree.Int(1)),
    Tree.Identifier("i"),
    Tree.Op(Tree.Identifier("i"), Token.Ops.Plus, Tree.Int(1))
  )

  property("simple.map") = AST.parseStatement("val a=map({0,1},i->i+1)") ?= Tree.Statement(
    Some("a"),
    Tree.Map(
      Tree.Range(Tree.Int(0), Tree.Int(1)),
      Tree.Identifier("i"),
      Tree.Op(Tree.Identifier("i"), Token.Ops.Plus, Tree.Int(1))
    )
  )
  property("nested.map") = AST.parseStatement("val a=map(map({0,1},i->i+1),i->i+1)") ?= Tree.Statement(
    Some("a"),
    Tree.Map(
      Tree.Map(
        Tree.Range(Tree.Int(0), Tree.Int(1)),
        Tree.Identifier("i"),
        Tree.Op(Tree.Identifier("i"), Token.Ops.Plus, Tree.Int(1))
      ),
      Tree.Identifier("i"),
      Tree.Op(Tree.Identifier("i"), Token.Ops.Plus, Tree.Int(1))
    )
  )
}

object ReduceProperties extends Properties("Reduce") {
  property("simple.reduce") = AST.reduceNode.parse("reduce({0,1},0,a b->a+b)").get.value ?= Tree.Reduce(
    Tree.Range(Tree.Int(0), Tree.Int(1)),
    Tree.Int(0),
    Tree.Identifier("a"),
    Tree.Identifier("b"),
    Tree.Op(Tree.Identifier("a"), Token.Ops.Plus, Tree.Identifier("b"))
  )

  property("simple.reduce") = AST.parseStatement("val a=reduce({0,1},0,a b->a+b)") ?= Tree.Statement(
    Some("a"),
    Tree.Reduce(
      Tree.Range(Tree.Int(0), Tree.Int(1)),
      Tree.Int(0),
      Tree.Identifier("a"),
      Tree.Identifier("b"),
      Tree.Op(Tree.Identifier("a"), Token.Ops.Plus, Tree.Identifier("b"))
    )
  )
  property("nested.reduce") = AST.parseStatement("val a=reduce({0,1},reduce({0,1},0,a b->a+b),a b->a+b)") ?= Tree
    .Statement(
      Some("a"),
      Tree.Reduce(
        Tree.Range(Tree.Int(0), Tree.Int(1)),
        Tree.Reduce(
          Tree.Range(Tree.Int(0), Tree.Int(1)),
          Tree.Int(0),
          Tree.Identifier("a"),
          Tree.Identifier("b"),
          Tree.Op(Tree.Identifier("a"), Token.Ops.Plus, Tree.Identifier("b"))
        ),
        Tree.Identifier("a"),
        Tree.Identifier("b"),
        Tree.Op(Tree.Identifier("a"), Token.Ops.Plus, Tree.Identifier("b"))
      )
    )
}

object OpesProperties extends Properties("Operation") {
  property("op.int,op,reduce") = {
    val prg = """4+reduce(sequence,0,x y->x+y)"""

    AST.opnode.parse(prg).get.value ?= Tree.Op(
      Tree.Int(4),
      Token.Ops.Plus,
      Tree.Reduce(
        Tree.Identifier("sequence"),
        Tree.Int(0),
        Tree.Identifier("x"),
        Tree.Identifier("y"),
        Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
      )
    )
  }

  property("op.(int),op,reduce") = {
    val prg = """(4)+reduce(sequence,0,x y->x+y)"""

    AST.opnode.parse(prg).get.value ?= Tree.Op(
      Tree.Int(4),
      Token.Ops.Plus,
      Tree.Reduce(
        Tree.Identifier("sequence"),
        Tree.Int(0),
        Tree.Identifier("x"),
        Tree.Identifier("y"),
        Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
      )
    )
  }

  property("op.(int),op,(reduce)") = {
    val prg = """(4)+(reduce(sequence,0,x y->x+y))"""

    AST.opnode.parse(prg).get.value ?= Tree.Op(
      Tree.Int(4),
      Token.Ops.Plus,
      Tree.Reduce(
        Tree.Identifier("sequence"),
        Tree.Int(0),
        Tree.Identifier("x"),
        Tree.Identifier("y"),
        Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
      )
    )
  }

  property("op.reduce,op,int") = {
    val prg = """reduce(sequence,0,x y->x+y)+4"""

    AST.opnode.parse(prg).get.value ?= Tree.Op(
      Tree.Reduce(
        Tree.Identifier("sequence"),
        Tree.Int(0),
        Tree.Identifier("x"),
        Tree.Identifier("y"),
        Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
      ),
      Token.Ops.Plus,
      Tree.Int(4)
    )
  }

  property("op.reduce,op,(int)") = {
    val prg = """reduce(sequence,0,x y->x+y)+(4)"""

    AST.opnode.parse(prg).get.value ?= Tree.Op(
      Tree.Reduce(
        Tree.Identifier("sequence"),
        Tree.Int(0),
        Tree.Identifier("x"),
        Tree.Identifier("y"),
        Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
      ),
      Token.Ops.Plus,
      Tree.Int(4)
    )
  }

  property("op.(reduce),op,(int)") = {
    val prg = """(reduce(sequence,0,x y->x+y))+(4)"""

    AST.opnode.parse(prg).get.value ?= Tree.Op(
      Tree.Reduce(
        Tree.Identifier("sequence"),
        Tree.Int(0),
        Tree.Identifier("x"),
        Tree.Identifier("y"),
        Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
      ),
      Token.Ops.Plus,
      Tree.Int(4)
    )
  }

  property("op.nested(reduce),op,(int)") = {
    val prg = """(reduce(sequence,reduce(sequence,0,x y->x+y),x y->x+y))+(4)"""

    val n1 = Tree.Reduce(
      Tree.Identifier("sequence"),
      Tree.Int(0),
      Tree.Identifier("x"),
      Tree.Identifier("y"),
      Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
    )
    val rn =
      Tree.Reduce(
        Tree.Identifier("sequence"),
        n1,
        Tree.Identifier("x"),
        Tree.Identifier("y"),
        Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
      )

    AST.opnode.parse(prg).get.value ?= Tree.Op(rn, Token.Ops.Plus, Tree.Int(4))
  }

  property("op.nested") = {
    val prg = """((-1+2.0)+i)+i"""

    val op1 = Tree.Op(Tree.Int(-1), Token.Ops.Plus, Tree.Float(2.0))
    val op2 = Tree.Op(op1, Token.Ops.Plus, Tree.Identifier("i"))
    val op3 = Tree.Op(op2, Token.Ops.Plus, Tree.Identifier("i"))

    AST.opnode.parse(prg).get.value ?= op3
  }

  property("op.nested") = {
    val prg = """(((-1+2.0)+i)+i)"""

    val op1 = Tree.Op(Tree.Int(-1), Token.Ops.Plus, Tree.Float(2.0))
    val op2 = Tree.Op(op1, Token.Ops.Plus, Tree.Identifier("i"))
    val op3 = Tree.Op(op2, Token.Ops.Plus, Tree.Identifier("i"))

    AST.opnode.parse(prg).get.value ?= op3
  }

  property("op.nested") = {
    val prg = """-1+2.0+i+i"""

    val op1 = Tree.Op(Tree.Int(-1), Token.Ops.Plus, Tree.Float(2.0))
    val op2 = Tree.Op(Tree.Identifier("i"), Token.Ops.Plus, Tree.Identifier("i"))
    val op3 = Tree.Op(op1, Token.Ops.Plus, op2)

    AST.opnode.parse(prg).get.value ?= op3
  }
}

object ProgramProperties extends Properties("Program") {
  property("simple.id,print,out") = {
    val prg =
      """
        |val n=500
        |print "pi ="
        |out n
      """.stripMargin

    AST.parseProgram(prg) ?= Seq(
      Tree.Statement(Some("n"), Tree.Int(500)),
      Tree.Statement(None, Tree.Print("pi =")),
      Tree.Statement(None, Tree.Out(Tree.Identifier("n")))
    )
  }

  property("simple.expr,op,range") = {
    val prg = """val sequence=4+reduce(sequence,0,x y->x+y)"""

    AST.parseProgram(prg) ?= Seq(
      Tree.Statement(
        Some("sequence"),
        Tree.Op(
          Tree.Int(4),
          Token.Ops.Plus,
          Tree.Reduce(
            Tree.Identifier("sequence"),
            Tree.Int(0),
            Tree.Identifier("x"),
            Tree.Identifier("y"),
            Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
          )
        )
      )
    )
  }

  property("simple.expr,op,range") = {
    val prg = """val sequence=reduce(sequence,0,x y->x+y)+4"""

    AST.parseProgram(prg) ?= Seq(
      Tree.Statement(
        Some("sequence"),
        Tree.Op(
          Tree.Reduce(
            Tree.Identifier("sequence"),
            Tree.Int(0),
            Tree.Identifier("x"),
            Tree.Identifier("y"),
            Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
          ),
          Token.Ops.Plus,
          Tree.Int(4)
        )
      )
    )
  }

  property("example.simple") = {
    val prg =
      """
        |val n=500
        |
        |val sequence=map({0,n},i->i+1)
        |val pi=reduce(sequence,0,x y->x+y)
        |
        |print "pi ="
        |out n
      """.stripMargin

    AST.parseProgram(prg) ?= Seq(
      Tree.Statement(Some("n"), Tree.Int(500)),
      Tree.Statement(
        Some("sequence"),
        Tree.Map(
          Tree.Range(Tree.Int(0), Tree.Identifier("n")),
          Tree.Identifier("i"),
          Tree.Op(Tree.Identifier("i"), Token.Ops.Plus, Tree.Int(1))
        )
      ),
      Tree.Statement(
        Some("pi"),
        Tree.Reduce(
          Tree.Identifier("sequence"),
          Tree.Int(0),
          Tree.Identifier("x"),
          Tree.Identifier("y"),
          Tree.Op(Tree.Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
        )
      ),
      Tree.Statement(None, Tree.Print("pi =")),
      Tree.Statement(None, Tree.Out(Tree.Identifier("n")))
    )
  }

  property("example.mail") = {
    val prg =
      """
        |val n=500
        |
        |val sequence=map({0,n},i->(-1+2.0)+i)
        |
        |val pi=4+reduce(sequence,0,x y->x+y)
        |
        |print "pi = "
        |
        |out pi
      """.stripMargin

    val n1 = Tree.Statement(Some("n"), Tree.Int(500))
    val n2 = Tree.Statement(
      Some("sequence"),
      Tree.Map(
        Tree.Range(Tree.Int(0), Tree.Identifier("n")),
        Tree.Identifier("i"),
        Tree.Op(Tree.Op(Tree.Int(-1), Token.Ops.Plus, Tree.Float(2.0)), Token.Ops.Plus, Tree.Identifier("i"))
      )
    )
    val n3 = Tree.Statement(
      Some("pi"),
      Tree.Op(
        Tree.Int(4),
        Token.Ops.Plus,
        Tree.Reduce(
          Tree.Identifier("sequence"),
          Tree.Int(0),
          Tree.Identifier("x"),
          Tree.Identifier("y"),
          Op(Identifier("x"), Token.Ops.Plus, Tree.Identifier("y"))
        )
      )
    )
    val n4 = Tree.Statement(None, Tree.Print("pi = "))
    val n5 = Tree.Statement(None, Tree.Out(Identifier("pi")))

    AST.parseProgram(prg) ?= Seq(n1, n2, n3, n4, n5)
  }
}
