package mapr
import org.scalacheck.Properties
import org.scalacheck.Prop._
import mapr.{Result => R}
import mapr.Result.{Eff => E}

object InterpreterProps extends Properties("Interpreter") {

  def printExpr(variable: String) = s"""$variable = """

  property("empty") = forAll { _: Boolean =>
    val prg = ""
    val res = Interpreter.interpret(AST.parseProgram(prg).toList)
    res ?= Vector()
  }

  property("simple program.int") = forAllNoShrink { i: Int =>
    val variable = "i"
    val prg =
      s"""
          |val $variable=$i
          |print "${printExpr(variable)}"
          |out $variable
        """.stripMargin
    val res = Interpreter.interpret(AST.parseProgram(prg).toList)

    res ?= Vector(E.Print(printExpr(variable)), E.Out(R.Val.Value(i.toDouble, Types.I)))
  }

  property("simple program.float") = forAllNoShrink { i: Double =>
    val variable = "i"
    val prg =
      f"""
         |val $variable=$i%2.2f
         |print "${printExpr(variable)}"
         |out $variable
        """.stripMargin
    val res = Interpreter.interpret(AST.parseProgram(prg).toList)

    res ?= Vector(E.Print(printExpr(variable)), E.Out(R.Val.Value(BigDecimal(i).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble, Types.F)))
  }

  property("complex program") = {
    val prg =
      """
        |val n=3
        |
        |val sequence=map({0,n},i->i)
        |print "sequence = "
        |out sequence
        |
        |val a = reduce(sequence,0.0,x y->x+y)
        |print "a = "
        |out a
        |
        |val pi=(4.0+reduce(sequence,0.0,x y->x+y))+a
        |
        |print "pi = "
        |out pi
        |""".stripMargin

    val res = Interpreter.interpret(AST.parseProgram(prg).toList)

    res ?= Vector(
      E.Print(printExpr("sequence")), E.Out(R.Val.Array(List(R.Val.Value(0, Types.I), R.Val.Value(1, Types.I), R.Val.Value(2, Types.I), R.Val.Value(3, Types.I)))),
      E.Print(printExpr("a")), E.Out(R.Val.Value(6.0, Types.F)),
      E.Print(printExpr("pi")), E.Out(R.Val.Value(16.0, Types.F)),
    )
  }
}
