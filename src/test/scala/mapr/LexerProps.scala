package mapr

import fastparse.core.Parsed
import org.scalacheck.Prop._
import org.scalacheck.{ Gen, Properties }

object LexerProps extends Properties("Lexer") {

  val idGen: Gen[String] =
    Gen.listOfN(5, Gen.oneOf(('a' to 'z') ++ ('A' to 'Z'))).map {
      case Nil    => Nil.mkString
      case h :: t => (h.toLower :: t).mkString
    }

  val printGen: Gen[String] = Gen
    .listOfN(29, Gen.oneOf(('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9') ++ " !@#$%^&*()_+=-[]{}\\/<>?,."))
    .map(_.mkString)

  val opGen: Gen[String] = Gen.oneOf(Seq("+", "-"))

  property("identifier") = forAllNoShrink(idGen) { in =>
    val str = s"val $in"
    val expected: Parsed[Token.Id, Char, String] = Parsed.Success(Token.Id(in), str.length)
    Lexer.identifier.parse(str) ?= expected
  }

  property("integer") = forAllNoShrink(Gen.chooseNum(Long.MinValue, Long.MaxValue)) { in =>
    val str = in.toString
    Lexer.intNumber.parse(str).get.value ?= Token.Int(in)
  }

  property("float") = forAllNoShrink(Gen.chooseNum(Double.MinValue, Double.MaxValue)) { in =>
    val str = f"$in%f"
    Lexer.floatNumber.parse(str).get.value ?= Token.Float(in)
  }

  property("print") = forAllNoShrink(printGen) { in: String =>
    val str = s"""print "$in""""
    Lexer.print.parse(str).get.value ?= Token.Print(in)
  }

  property("out") = forAllNoShrink(idGen) { in: String =>
    val str = s"out $in"
    Lexer.out.parse(str).get.value ?= Token.Out(in)
  }

  property("map") = Lexer.map.parse("map").get.value ?= Token.Map

  property("reduce") = Lexer.reduce.parse("reduce").get.value ?= Token.Reduce

  property("tokens.oneline.id") = Lexer.tokenize("val n = 500") ?= List(Token.Id("n"), Token.Equal, Token.Int(500))

  property("tokens.oneline.lambda") = Lexer.tokenize("a b -> a + b") ?= List(
    Token.Id("a"),
    Token.Id("b"),
    Token.Arrow,
    Token.Id("a"),
    Token.Ops.Plus,
    Token.Id("b")
  )

  property("tokens.oneline.range") = Lexer.tokenize("{0, 10}") ?= List(
    Token.LCParent,
    Token.Int(0),
    Token.Comma,
    Token.Int(10),
    Token.RCParent
  )

  property("tokens.oneline.map") = Lexer.tokenize("map({0, 10}, i -> i)") ?= List(
    Token.Map,
    Token.LParent,
    Token.LCParent,
    Token.Int(0),
    Token.Comma,
    Token.Int(10),
    Token.RCParent,
    Token.Comma,
    Token.Id("i"),
    Token.Arrow,
    Token.Id("i"),
    Token.RParent
  )

  property("tokens.oneline.reduce") = Lexer.tokenize("reduce({0, 10}, 0, a  b -> a + b)") ?= List(
    Token.Reduce,
    Token.LParent,
    Token.LCParent,
    Token.Int(0),
    Token.Comma,
    Token.Int(10),
    Token.RCParent,
    Token.Comma,
    Token.Int(0),
    Token.Comma,
    Token.Id("a"),
    Token.Id("b"),
    Token.Arrow,
    Token.Id("a"),
    Token.Ops.Plus,
    Token.Id("b"),
    Token.RParent
  )

  property("tokens.expression.(float)") = Lexer.tokenize("(2.0)") ?= List(
    Token.LParent,
    Token.Float(2.0),
    Token.RParent
  )

  property("tokens.expression.(float - id + int)") = Lexer.tokenize("(2.0 - i + 1)") ?= List(
    Token.LParent,
    Token.Float(2.0),
    Token.Ops.Minus,
    Token.Id("i"),
    Token.Ops.Plus,
    Token.Int(1),
    Token.RParent
  )

  property("tokens.assignment.map_expr") = Lexer.tokenize("val sequence = map({0, n}, i -> (-1)+i - (2.0 - i + 1))") ?= List(
    Token.Id("sequence"),
    Token.Equal,
    Token.Map,
    Token.LParent,
    Token.LCParent,
    Token.Int(0),
    Token.Comma,
    Token.Id("n"),
    Token.RCParent,
    Token.Comma,
    Token.Id("i"),
    Token.Arrow,
    Token.LParent,
    Token.Int(-1),
    Token.RParent,
    Token.Ops.Plus,
    Token.Id("i"),
    Token.Ops.Minus,
    Token.LParent,
    Token.Float(2.0),
    Token.Ops.Minus,
    Token.Id("i"),
    Token.Ops.Plus,
    Token.Int(1),
    Token.RParent,
    Token.RParent
  )

  property("tokens.example") = {
    val code =
      """
        |val n = 500
        |
        |val sequence = map({0, n}, i -> (-1)+i - (2.0 + i + 1))
        |
        |val pi = 4 + reduce(sequence, 0, x y -> x + y)
        |
        |print "pi = "
        |
        |out pi
      """.stripMargin

    Lexer.tokenize(code) ?= List(
      Token.Id("n"),
      Token.Equal,
      Token.Int(500),
      Token.Id("sequence"),
      Token.Equal,
      Token.Map,
      Token.LParent,
      Token.LCParent,
      Token.Int(0),
      Token.Comma,
      Token.Id("n"),
      Token.RCParent,
      Token.Comma,
      Token.Id("i"),
      Token.Arrow,
      Token.LParent,
      Token.Int(-1),
      Token.RParent,
      Token.Ops.Plus,
      Token.Id("i"),
      Token.Ops.Minus,
      Token.LParent,
      Token.Float(2.0),
      Token.Ops.Plus,
      Token.Id("i"),
      Token.Ops.Plus,
      Token.Int(1),
      Token.RParent,
      Token.RParent,
      Token.Id("pi"),
      Token.Equal,
      Token.Int(4),
      Token.Ops.Plus,
      Token.Reduce,
      Token.LParent,
      Token.Id("sequence"),
      Token.Comma,
      Token.Int(0),
      Token.Comma,
      Token.Id("x"),
      Token.Id("y"),
      Token.Arrow,
      Token.Id("x"),
      Token.Ops.Plus,
      Token.Id("y"),
      Token.RParent,
      Token.Print("pi = "),
      Token.Out("pi")
    )
  }
}
