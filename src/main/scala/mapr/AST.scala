package mapr

sealed trait Tree extends Product with Serializable
object Tree {
  final case class Int(value: Long) extends Tree
  final case class Float(value: Double) extends Tree

  final case class Op(left: Tree, token: Token, right: Tree) extends Tree

  final case class Identifier(value: String) extends Tree
  final case class Out(expr: Tree) extends Tree
  final case class Print(text: String) extends Tree

  final case class Range(start: Tree, end: Tree) extends Tree
  final case class Arr(values: List[Tree]) extends Tree
  final case class Map(values: Tree, id: Tree.Identifier, fn: Tree) extends Tree
  final case class Reduce(values: Tree, zero: Tree, prev: Tree.Identifier, next: Tree.Identifier, fn: Tree) extends Tree

  final case class Statement(id: Option[String], expr: Tree) extends Tree
}

object AST {
  import Lexer._
  import fastparse.all._

  lazy val opnode_1: P[Tree.Op] = P(expr_1 ~ op ~ expr_1).map {
    case (l, t, r) => Tree.Op(l, t, r)
  }

  lazy val opnode_2: P[Tree.Op] = P(opnode_1 ~ op ~ opnode_1).map {
    case (l, t, r) => Tree.Op(l, t, r)
  }

  lazy val opnode_3: P[Tree.Op] = P(opnode_2 | opnode_1)
  lazy val opnode: P[Tree.Op] = opnode_3 | P(lpar ~ opnode_3 ~ rpar).map(_._2)

  lazy val inpar: P[Tree] = P(lpar ~ expr ~ rpar).map(_._2)

  lazy val identifierNode: P[Tree.Identifier] = identifier.map(t => Tree.Identifier(t.value))

  lazy private val intNode = intNumber.map(t => Tree.Int(t.value))
  lazy private val floatNode = floatNumber.map(t => Tree.Float(t.value))
  lazy val number: P[Tree] = floatNode | intNode

  lazy val expr_1: P[Tree] = P(number | inpar | reduceNode | identifierNode)

  lazy val range: P[Tree.Range] = P(lcpar ~ expr ~ comma ~ expr ~ rcpar).map {
    case (_, s, _, e, _) => Tree.Range(s, e)
  }

  lazy val mapNode: P[Tree.Map] = P(map ~ lpar ~ expr ~ comma ~ identifier ~ arrow ~ expr ~ rpar).map {
    case (_, _, values, _, id, _, fn, _) => Tree.Map(values, Tree.Identifier(id.value), fn)
  }

  lazy val reduceNode: P[Tree.Reduce] =
    P(reduce ~ lpar ~ expr ~ comma ~ expr ~ comma ~ identifier ~ space ~ identifier ~ arrow ~ expr ~ rpar).map {
      case (_, _, values, _, zero, _, prev, next, _, fn, _) => Tree.Reduce(values, zero, Tree.Identifier(prev.value), Tree.Identifier(next.value), fn)
    }

  lazy val expr: P[Tree] = P(mapNode | opnode | reduceNode | expr_1 | range)

  def stmt: P[Tree.Statement] = {
    def case1 =
      P(identifier ~ space.rep(max = 100) ~ equal ~ space.rep(max = 100) ~ expr)
        .map(p => Tree.Statement(Some(p._1.value), p._3))
    def case2 = P(out).map(t => Tree.Statement(None, Tree.Out(Tree.Identifier(t.id))))
    def case3 = P(print).map(p => Tree.Statement(None, Tree.Print(p.text)))

    case1 | case2 | case3
  }

  lazy val statement: P[Tree.Statement] = P(Start ~ stmt ~ End)
  lazy val program: P[Seq[Tree.Statement]] = P(
    Start ~ (space.rep(max = 100).? ~ stmt ~ space.rep(max = 100).?).rep ~ End
  )

  def parseStatement(code: String): Tree.Statement = statement.parse(code).get.value
  def parseProgram(code: String): Seq[Tree.Statement] = program.parse(code).get.value
}
