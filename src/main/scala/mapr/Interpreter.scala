package mapr
import cats.Eval
import cats.data.State

import scala.annotation.tailrec

sealed trait Types extends Product with Serializable
object Types {
  final case object F extends Types
  final case object I extends Types
}

sealed trait Result extends Product with Serializable
object Result {
  sealed trait Val extends Result
  object Val {
    final case class Value(value: Double, `type`: Types) extends Val
    final case class Array(values: List[Val]) extends Val
  }

  sealed trait Eff extends Result
  object Eff {
    final case object Undefined extends Eff
    final case class Print(value: String) extends Eff
    final case class Out(value: Result) extends Eff
  }
}

final case class Scope(variables: Map[String, Result]) extends AnyVal {
  def updated(id: String, result: Result) = Scope(variables.updated(id, result))
}
object Scope {
  val empty = Scope(variables = Map.empty)
}

object Interpreter {

  /**
    * @param vs     A Tree.Arr to interpret
    * @param scope  A scope to interpret the statement of the Tree.Arr
    * @param f      A function to apply to an element of the Tree.Arr
    * @return
    */
  def interpretTreeArray(vs: Tree.Arr, scope: Scope, f: Tree => Tree): List[Result.Val] =
    vs.values.foldRight(List.empty[Result.Val]) {
      case (t, acc) =>
        interpretStatement.runA((f(t), scope)).value match {
          case r: Result.Val => r :: acc
          case other         => throw new Error(s"Result is not a value or array: $other")
        }
    }

  def interpretStatement: State[(Tree, Scope), Result] = State {
    case (expr @ Tree.Statement(Some(id), inexpr), scope) =>
      inexpr match {
        case Tree.Identifier(value) =>
          scope.variables.get(value) match {
            case Some(res) => ((expr, scope.updated(id, res)), res)
            case None      => throw new Error(s"${value} is not defined")
          }

        case _ =>
          val res = interpretStatement.runA((inexpr, scope)).value
          ((expr, scope.updated(id, res)), res)
      }

    case state @ (Tree.Statement(None, expr), scope) =>
      expr match {
        case out: Tree.Out     => (state, Result.Eff.Out(interpretStatement.runA((out, scope)).value))
        case print: Tree.Print => (state, Result.Eff.Print(print.text))
        case _                 => (state, Result.Eff.Undefined)
      }

    case state @ (Tree.Out(result), scope) =>
      result match {
        case Tree.Identifier(id) =>
          scope.variables.get(id) match {
            case Some(res: Result.Val.Value) => (state, res)
            case Some(res: Result.Val.Array) => (state, res)
            case None                        => throw new Error(s"$id is not defined")
            case Some(res)                   => throw new Error(s"$res is an eff")
          }
        case other => throw new Error(s"Out should be a variable identifier: $other")
      }

    case state @ (Tree.Print(text), _) =>
      (state, Result.Eff.Print(text))

    case state @ (Tree.Int(value), _) =>
      (state, Result.Val.Value(value.toDouble, Types.I))

    case state @ (Tree.Float(value), _) =>
      (state, Result.Val.Value(value, Types.F))

    case state @ (tr @ Tree.Arr(xs), scope) =>
      val rs = interpretTreeArray(tr, scope, identity)
      (state, Result.Val.Array(rs))

    case state @ (Tree.Op(l, op, r), scope) =>
      val er: Eval[Result] = for {
        lr <- interpretStatement.runA((l, scope))
        rr <- interpretStatement.runA((r, scope))
      } yield
        (lr, rr) match {
          case (Result.Val.Value(left, lt), Result.Val.Value(right, rt)) if lt == rt =>
            val res = op match {
              case Token.Ops.Plus  => Result.Val.Value(left + right, lt)
              case Token.Ops.Minus => Result.Val.Value(left - right, lt)
              case _               => throw new Exception(s"Token is not represent an operation: $op")
            }
            res

          case (Result.Val.Value(left, lt), Result.Val.Value(right, rt)) =>
            throw new Exception(s"Operand types are not compatible: ($left:$lt) $op ($right:$rt)")

          case (_: Result.Eff, _) => throw new Exception("Left side of the operation is not a value")
          case (_, _: Result.Eff) => throw new Exception("Right side of the operation is not a value")
          case other              => throw new Exception(s"Operation can not be interpreted: $other")
        }

      (state, er.value)

    case state @ (Tree.Range(start, end), scope) =>
      val res = for {
        sr <- interpretStatement.runA((start, scope))
        er <- interpretStatement.runA((end, scope))
      } yield
        (sr, er) match {
          case (Result.Val.Value(s, Types.I), Result.Val.Value(e, Types.I)) if s < e =>
            Result.Val.Array(Range.inclusive(s.toInt, e.toInt).map(v => Result.Val.Value(v.toDouble, Types.I)).toList)
          case (Result.Val.Value(s, Types.I), Result.Val.Value(e, Types.I)) if s > e =>
            throw new Exception(s"Range start should be <= end: $s > $e")
          case (Result.Val.Value(s, st), Result.Val.Value(e, et)) =>
            throw new Exception(s"Range start and end should be integers: $s:$st, $e:$et")
          case _ => throw new Exception(s"Something went wrong: $state")
        }

      (state, res.value)

    case state @ (Tree.Map(values, id, fn), scope) =>
      val res = values match {
        case arrt @ Tree.Arr(vs) =>
          val rs = interpretTreeArray(arrt, scope, t => replace(id, t, fn))
          Result.Val.Array(rs)

        case Tree.Range(start, end) =>
          val vs = for {
            sr <- interpretStatement.runA((start, scope))
            er <- interpretStatement.runA((end, scope))
          } yield
            (sr, er) match {
              case (Result.Val.Value(s, Types.I), Result.Val.Value(e, Types.I)) if s < e =>
                Tree.Arr(Range.inclusive(s.toInt, e.toInt).map(v => Tree.Int(v.toLong)).toList)
              case (Result.Val.Value(s, Types.I), Result.Val.Value(e, Types.I)) if s > e =>
                throw new Exception(s"Range start should be <= end: $s > $e")
              case (Result.Val.Value(s, st), Result.Val.Value(e, et)) =>
                throw new Exception(s"Range start and end should be integers: $s:$st, $e:$et")
              case _ => throw new Exception(s"Something went wrong: $state")
            }
          val rs = interpretTreeArray(vs.value, scope, t => replace(id, t, fn))
          Result.Val.Array(rs)

        case other => interpretStatement.runA((other, scope)).value // throw new NotImplementedError(s"$other")
      }

      (state, res)

    case state @ (Tree.Reduce(values, zero, prev, next, fn), scope) =>
      @tailrec
      def go(vs: List[Tree], accExpr: Tree, acc: Result.Val.Value): Result.Val.Value = vs match {
        case Nil => acc

        case h :: t =>
          val accRepl = replace(prev, accExpr, fn)
          val expr = replace(next, h, accRepl)
          interpretStatement.runA((expr, scope)).value match {
            case r: Result.Val.Value => go(t, expr, r)
            case _                   => ???
          }
      }

      val res = values match {
        case Tree.Arr(vs) =>
          interpretStatement.runA((zero, scope)).value match {
            case acc: Result.Val.Value => go(vs, zero, acc)
            case _                     => ???
          }

        case Tree.Range(start, end) =>
          val vs = for {
            sr <- interpretStatement.runA((start, scope))
            er <- interpretStatement.runA((end, scope))
          } yield
            (sr, er) match {
              case (Result.Val.Value(s, Types.I), Result.Val.Value(e, Types.I)) if s < e =>
                Tree.Arr(Range.inclusive(s.toInt, e.toInt).map(v => Tree.Int(v.toLong)).toList)
              case (Result.Val.Value(s, Types.I), Result.Val.Value(e, Types.I)) if s > e =>
                throw new Exception(s"Range start should be <= end: $s > $e")
              case (Result.Val.Value(s, st), Result.Val.Value(e, et)) =>
                throw new Exception(s"Range start and end should be integers: $s:$st, $e:$et")
              case _ => throw new Exception(s"Something went wrong: $state")
            }

          val res = interpretStatement.runA((zero, scope)).value match {
            case acc: Result.Val.Value => go(vs.value.values, zero, acc)
            case _                     => ???
          }

          res

        case m: Tree.Map =>
          val res = interpretStatement.runA((m, scope)).value
          res match {
            case Result.Val.Array(vs) =>
              val tree = Tree.Arr(vs.map {
                case v: Result.Val.Value => Tree.Float(v.value)
                case v                   => ???
              })
              interpretStatement.runA((Tree.Reduce(tree, zero, prev, next, fn), scope)).value
            case _ => ???
          }

        case other =>
          val res = interpretStatement.runA((other, scope)).value match {
            case Result.Val.Array(vs) =>
              val tree = Tree.Arr(vs.map {
                case v: Result.Val.Value => Tree.Float(v.value)
                case v                   => ???
              })
              interpretStatement.runA((Tree.Reduce(tree, zero, prev, next, fn), scope)).value
            case _ => ???
          }
          res
      }

      (state, res)

    case state @ (Tree.Identifier(id), scope) => (state, scope.variables(id))
  }

  def replace(id: Tree.Identifier, subst: Tree, orig: Tree): Tree = orig match {
    case _: Tree.Identifier if orig == id        => subst
    case _: Tree.Identifier                      => orig
    case f: Tree.Float                           => f
    case i: Tree.Int                             => i
    case a: Tree.Arr                             => Tree.Arr(a.values.map(v => replace(id, subst, v)))
    case o: Tree.Out                             => o
    case p: Tree.Print                           => p
    case Tree.Op(l, t, r)                        => Tree.Op(replace(id, subst, l), t, replace(id, subst, r))
    case Tree.Range(s, e)                        => Tree.Range(replace(id, subst, s), replace(id, subst, e))
    case Tree.Statement(i, expr)                 => Tree.Statement(i, replace(id, subst, expr))
    case m @ Tree.Map(values, i, fn)             => m // Tree.Map(values, i, replace(id, subst, fn))
    case r @ Tree.Reduce(values, zero, p, n, fn) => r // Tree.Reduce(values, zero, replace(id, subst, fn))
  }

  def interpret(program: List[Tree]): Vector[Result] = {
    @tailrec
    def go(prg: List[Tree], scope: Scope, instructions: Vector[Result]): Vector[Result] = prg match {
      case Nil => instructions
      case h :: t =>
        val ((_, s), r) = interpretStatement.run((h, scope)).value
        r match {
          case eff: Result.Eff => go(t, s, instructions :+ eff)
          case _               => go(t, s, instructions)
        }
    }

    go(program, Scope.empty, Vector.empty)
  }
}
