package mapr

import fastparse.all._

sealed abstract class Token extends Product with Serializable
object Token {
  final case class Id(value: String) extends Token
  final case class Int(value: Long) extends Token
  final case class Float(value: Double) extends Token
  final case class Print(text: String) extends Token
  final case class Out(id: String) extends Token
  final case object Map extends Token
  final case object Reduce extends Token
  final case object LParent extends Token
  final case object RParent extends Token
  final case object LCParent extends Token
  final case object RCParent extends Token
  final case object Equal extends Token
  final case object Arrow extends Token
  final case object Comma extends Token

  sealed abstract class Ops extends Token
  object Ops {
    final case object Plus extends Ops
    final case object Minus extends Ops
  }
}

object Lexer {
  private val alphaStr = P(
    CharIn('a' to 'z') | CharIn('A' to 'Z') | CharIn('0' to '9') | CharIn(" !@#$%^&*()_+=-[]{}\\/<>?,.")
  )
  val space = P(CharsWhileIn(" \r\n").?)
  private val digits = P(CharsWhileIn("0123456789"))
  private val fractional = P("." ~ digits)
  private val integral = P("0" | CharIn('1' to '9') ~ digits.?)
  private val idChars = P(CharIn('a' to 'z') ~ (CharIn('a' to 'z') | CharIn('A' to 'Z') | CharIn('0' to '9')).rep)

  val identifier: P[Token.Id] = P(("val " ~ idChars.!) | idChars.!).map(Token.Id)

  val intNumber: P[Token.Int] = P(CharIn("+-").? ~ integral).!.map(
    x => Token.Int(x.toLong)
  )
  val floatNumber: P[Token.Float] =
    P(CharIn("+-").? ~ integral ~ fractional).!.map(
      x => Token.Float(x.toDouble)
    )

  val map: P[Token.Map.type] = P("map").map(_ => Token.Map)
  val reduce: P[Token.Reduce.type] = P("reduce").map(_ => Token.Reduce)
  val lpar: P[Token.LParent.type] = P("(").map(_ => Token.LParent)
  val rpar: P[Token.RParent.type] = P(")").map(_ => Token.RParent)
  val lcpar: P[Token.LCParent.type] = P("{").map(_ => Token.LCParent)
  val rcpar: P[Token.RCParent.type] = P("}").map(_ => Token.RCParent)
  val equal: P[Token.Equal.type] = P("=").map(_ => Token.Equal)
  val arrow: P[Token.Arrow.type] = P("->").map(_ => Token.Arrow)
  val comma: P[Token.Comma.type] = P(",").map(_ => Token.Comma)

  val print: P[Token.Print] = {
    import fastparse.all._ // ignore White._
    P("print " ~ "\"" ~ alphaStr.rep.! ~ "\"").map(Token.Print)
  }

  val out: P[Token.Out] = P("out" ~ space ~ idChars.!).map(Token.Out)

  val op: P[Token.Ops] = P(CharIn("+-")).!.map {
    case "+" => Token.Ops.Plus
    case "-" => Token.Ops.Minus
  }

  val token: P[Token] =
    (floatNumber | intNumber) | print | out | map | reduce | identifier | lpar | rpar | lcpar | rcpar | equal | arrow | op | comma

  val ts: P[Seq[Token]] = (space.rep(max = 1000) ~ token ~ space.rep(max = 1000)).rep

  val program: P[Seq[Token]] =
    P(Start ~ ts ~ End)

  def tokenize(str: String): Seq[Token] = program.parse(str).get.value
}
