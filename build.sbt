name := "mapr"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "com.lihaoyi"    %% "fastparse"  % "1.0.0",
  "org.typelevel"  %% "cats-core"  % "1.2.0",
  "org.scalacheck" %% "scalacheck" % "1.14.0",
  compilerPlugin("org.spire-math"  %% "kind-projector" % "0.9.7"),
  compilerPlugin("org.scalamacros" %% "paradise"       % "2.1.1" cross CrossVersion.full)
)

scalacOptions ++= Seq(
  "-explaintypes",
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-unchecked",
  "-Xfuture",
  "-Xfatal-warnings",
  "-Xlint:by-name-right-associative",
  "-Xlint:unsound-match",
  "-Xlint:inaccessible",
  "-Yrangepos",
  "-Ywarn-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-inaccessible",
  "-Ywarn-nullary-override",
  "-Ywarn-numeric-widen",
  "-Ywarn-infer-any",
  "-Ypartial-unification",
  "-language:experimental.macros",
  "-language:implicitConversions"
)
